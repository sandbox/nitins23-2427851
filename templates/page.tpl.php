<?php
/**
 * @file
 * Custom theme implementation to display a single Drupal page.
 */
?>

<!-- Header -->
<header id="header">
  <div class="intro-header">

    <?php if ($logo): ?>
      <div id="logo"><a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><img src="<?php print $logo; ?>"/></a></div>
    <?php endif; ?>
    <h1 id="site-title">
      <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>"><?php print $site_name; ?></a>
      <div id="site-description"><?php print $site_slogan; ?></div>
    </h1>
    <?php if ($navbar_menu): ?>
      <nav id="nav">
        <?php print $navbar_menu ?>
      </nav>
    <?php endif ?>
  </div>
</header>

<!-- Main -->
<section id="main">
  <?php print render($page['content']) ?>
  <?php print render($page['content-top']) ?>
  <?php print render($page['content-bottom']) ?>
   <?php print render($page['banner']) ?>
</section>

<!-- Footer -->
<footer>
  <?php if ($page['footer']): ?>
    <?php print render($page['footer']) ?>
  <?php endif ?>
  <ul class="list-inline">
    <li><a href="#">Home</a></li>
    <li class="footer-menu-divider">⋅</li>
    <li><a href="#services">Services</a></li> 
     <li class="footer-menu-divider">⋅</li>
    <li><a href="#contact">Contact</a></li>
  </ul>
  <ul class="copyright text-muted small">
    <li>&copy; Untitled. All rights reserved.</li><li>Design: <a href="https://www.drupal.org/u/nitin-shirole">Nitin</a></li>
  </ul>
</footer>

